﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform player_pos;
    Vector3 difference = Vector3.zero;
	
	void Start () {
        difference = transform.position - player_pos.position;
	}
	
	void LateUpdate () {
        transform.position = player_pos.position+ difference;
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            Destroy(other.gameObject); //only destroy this thing if it is tagged "pickup"
        }
       
    }

    private void OnCollisionEnter(Collision collision) //no physics; for collisions
    {
        
    }
}
