﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float horizontal = Input.GetAxis("Horizontal");//going to be some number between -1 and 1
        float vertical = Input.GetAxis("Vertical");

        GetComponent<Rigidbody>().AddForce(new Vector3(horizontal, 0, vertical));
	}
}

